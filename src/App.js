// import logo from './logo.svg';
// import './App.css';
import React from 'react';
import Header from './Header/Header';
import Menu from './Menu Tab/Menu';
import Footer from './Footer/Footer';
// import Appnavbar from './Appnavbar'

import "./App.css";

function App() {
  return (
    <div>
      <Header />
      <Menu />
      <Footer />
    </div>
  );
}

export default App;
